import {Shape, ShapeUtils, ShapeGeometry, BufferGeometry, BufferAttribute, Float32BufferAttribute, DynamicDrawUsage} from 'three';

class HexagonsSquareGeometry extends ShapeGeometry {

    constructor(square=2048, rate=80, separation=1, freq=0.1, heightRange=20, curveSegments=6) {

        super();
        this.type = 'ShapeGeometry';

        this.parameters = {
            rate: rate,
            square: square,
            separation: separation,
            curveSegments: curveSegments,
            shapes: []

        };

        let hexRadius = Math.floor(square / rate);
        const hexHeight = hexRadius * (2 - Math.sin(Math.PI / 3 / 2));
        const hexWidth = hexRadius * Math.cos(Math.PI / 3 / 2) * 2;

        this.hexCountX = Math.floor(square / hexWidth / 2);
        this.hexCountZ = Math.floor(square / hexHeight / 2);

        const hexOffsetX = -(this.hexCountX * hexWidth - square);
        const hexOffsetZ = -(this.hexCountZ * hexHeight - square);

        hexRadius = hexRadius * separation;

        const hex_vertices = [];

        const indices = [];
        const vertices = [];
        const normals = [];
        const uvs = [];

        let groupStart = 0;
        let groupCount = 0;
        let hexCount = 0;

        let dx, dz;

        for (var i = 0; i < 6; i++) {

            hex_vertices.push({

                x: hexRadius * Math.sin(i * Math.PI / 3),
                z: -hexRadius * Math.cos(i * Math.PI / 3)
            });
        }

        for (let x = -this.hexCountX; x <= this.hexCountX; x++) {

            for (let z = -this.hexCountZ; z <= this.hexCountZ; z++) {

                dx = 2 * hexOffsetX + (x + 0.5) * hexWidth - (Math.abs(z % 2) == 1 ? 0 : hexWidth / 2);
                dz = 2 * hexOffsetZ + (z + 0.5) * hexHeight;

                const hexShape = new Shape();
                hexShape.moveTo(hex_vertices[0].x - dx, hex_vertices[0].z - dz);

                for (var i = 1; i < 6; i++)
                    hexShape.lineTo(hex_vertices[i].x - dx, hex_vertices[i].z - dz);

                this.parameters.shapes.push(hexShape);

                addShape(hexShape);

                this.addGroup(groupStart, groupCount, i);

                groupStart += groupCount;
                groupCount = 0;

                hexCount++;

            }
        }

        function addShape(shape) {

            const indexOffset = vertices.length / 3;
            const points = shape.extractPoints(curveSegments);

            let shapeVertices = points.shape;
            const shapeHoles = points.holes;

            // check direction of vertices

            if (ShapeUtils.isClockWise(shapeVertices) === false) {

                shapeVertices = shapeVertices.reverse();

            }

            for (let i = 0, l = shapeHoles.length; i < l; i++) {

                const shapeHole = shapeHoles[i];

                if (ShapeUtils.isClockWise(shapeHole) === true) {

                    shapeHoles[i] = shapeHole.reverse();

                }

            }

            const faces = ShapeUtils.triangulateShape(shapeVertices, shapeHoles);

            // join vertices of inner and outer paths to a single array

            for (let i = 0, l = shapeHoles.length; i < l; i++) {

                const shapeHole = shapeHoles[i];
                shapeVertices = shapeVertices.concat(shapeHole);

            }

            // vertices, normals, uvs

            for (let i = 0, l = shapeVertices.length; i < l; i++) {

                const vertex = shapeVertices[i];

                vertices.push(vertex.x, vertex.y, 0);
                normals.push(0, 0, 1);
                uvs.push(vertex.x, vertex.y);
                // world uvs

            }

            // incides

            for (let i = 0, l = faces.length; i < l; i++) {

                const face = faces[i];

                const a = face[0] + indexOffset;
                const b = face[1] + indexOffset;
                const c = face[2] + indexOffset;

                indices.push(a, b, c);
                groupCount += 3;

            }

        }

        // build geometry

        var colors = new Float32Array(vertices.length * 4 / 3).fill(1);

        //           att_col.setUsage( DynamicDrawUsage );
        //           att_pos.setUsage( DynamicDrawUsage );

        let vi = 2
          , j = 3;

        for (let ix = -this.hexCountX; ix <= this.hexCountX; ix++) {

            for (let iy = -this.hexCountZ; iy <= this.hexCountZ; iy++) {

                const dY = Math.cos(Math.sqrt(freq * Math.pow(ix, 2) + freq * Math.pow(iy, 2)));

                for (var v = 1; v < 7; v++) {

                    vertices[vi] = dY * heightRange;

                    colors[j - 3] = dY;
                    colors[j - 2] = 1 - dY * dY;
                    colors[j - 1] = 1 - dY * dY;
                    colors[j] = 1;

                    vi += 3;
                    j += 4;

                }

            }

        }

        this.setIndex(indices);
        this.setAttribute('position', new Float32BufferAttribute(vertices,3));
        this.setAttribute('normal', new Float32BufferAttribute(normals,3));
        this.setAttribute('color', new Float32BufferAttribute(colors,4));
        this.setAttribute('uv', new Float32BufferAttribute(uvs,2));

    }

}

export {HexagonsSquareGeometry};
