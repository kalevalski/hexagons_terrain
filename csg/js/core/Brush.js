import { Mesh, Matrix4 } from 'three';
import { MeshBVH } from './MeshBVH.js';
import { HalfEdgeMap } from './HalfEdgeMap.js';


export class Brush extends Mesh {

	constructor( ...args ) {

		super( ...args );

		this.isBrush = true;
		this._previousMatrix = new Matrix4();
		this._previousMatrix.elements.fill( 0 );

	}

	prepareGeometry() {

		const geometry = this.geometry;

		if ( ! geometry.boundsTree ) {

			geometry.boundsTree = new MeshBVH( geometry, { maxLeafTris: 3 } ); //argh 3
			if ( geometry.halfEdges ) {

				geometry.halfEdges.updateFrom( geometry );

			}

		}

		if ( ! geometry.halfEdges ) {

			geometry.halfEdges = new HalfEdgeMap( geometry );

		}

	}

}
